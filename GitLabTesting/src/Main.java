
public class Main {
	
	private String input;
	
	public Main () {
		this.input = "Hello, world!";
	}	
	
	public Main (String inInput) {
		this.input = inInput;
	}
	
	private void sayInput () {
		System.out.println(input);
	}
		
	public static void main (String[] args) {
		
		Main main = null;
		
		if (args.length > 0) {
			main = new Main(args[0]);
		} else {
			main = new Main();
		}
		
		main.sayInput();
		
	}
	
}
